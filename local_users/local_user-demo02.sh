#!/bin/bash

## Display UID & username of current user (executing this script)

### Display UID
echo "Your UID is: ${UID}"

### Display username
USER_NAME=$(id -un)
# USER_NAME=`id -un`
echo "Your username is: $USER_NAME"


## Display if user is root (or not)
if [[ "$UID" -eq 0 ]]
then
    echo "You have root privileges"
else
    echo "You do not have root privileges"
fi
