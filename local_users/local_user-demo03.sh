#!/bin/bash

## Display UID & username of current user (executing this script) & if user is vagrant or not

### Display UID
echo "Your UID is: ${UID}"
### Only display if UID does NOT match 1000
VALID_UID='1000'
if [[ "${UID}" -ne "${VALID_UID}" ]]
then
    echo "ERROR: your UID does not match $VALID_UID"
    exit 1
fi


## Display username
USER_NAME=$(id -un)
### Test if the command succeeded (test exit status of previous command)
if [[ "${?}" -ne 0 ]]
then
    echo "the '$(id -un)'-command did not execute succesfully"
    exit 1
fi


## Test if username = vagrant
echo "Your username: $USER_NAME"
VALID_USER_NAME="vagrant"
### = checks value, == checks pattern
if [[ "${USER_NAME}" = "${VALID_USER_NAME}" ]]
then
    echo "username is valid"
fi


## Test if username != vagrant
if [[ "${USER_NAME}" != "${VALID_USER_NAME}" ]]
then
    echo "username is invalid"
fi