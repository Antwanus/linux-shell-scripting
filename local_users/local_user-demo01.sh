#!/bin/bash

# This script displays various info to the screen

## Display 'hello'
echo 'Hello'

## Assign a variable
WORD='bird'

## Display a variable (double quotes!!)
echo "$WORD"

### concat variable with hard-coded text
echo "$WORD is the WORD"

### concat differently
echo "${WORD}_is_still_the_word"

WORD_X_2='birdbird'

echo "${WORD}${WORD_X_2}"
