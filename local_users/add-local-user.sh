#!/bin/bash

#
#   This script creates a new user on the local system.
#       - user will be prompted for username (login), real name (comment) and a password.
#       - the username, password & host for the account will be displayed.
#


# check if user is root
echo "Your UID is: ${UID}"
if [[ ${UID} -ne 0 ]]
then
    echo "ERROR: This script requires root privileges"
    exit 1
else
    echo "user is root..."
fi


# get input
read -p "Enter user_name: " USER_NAME
read -p "Enter real_name/application_name: " REAL_NAME
read -p "Enter password: " PASSWORD


# create account
useradd -c "${REAL_NAME}" -m $USER_NAME
# check if prev command has exit 0 (=success)
if [[ "${?}" -ne 0 ]]
then
    echo "The account could not be created"
    exit 1
else
    echo "Account created..."
fi


# set password
echo ${PASSWORD} | passwd --stdin $USER_NAME
# check if prev command has exit 0 (=success)
if [[ "${?}" -ne 0 ]]
then
    echo "The password could not be created"
    exit 1
else
    echo "Password is set..."
fi


# force password change on 1st login
passwd -e ${USER_NAME}
if [[ "${?}" -ne 0 ]]
then
    echo "The password could not be expired"
    exit 1
else
    echo "Password expired..."
fi


# display user, pw & host
echo "-------Summary--------"
echo "username: ${USER_NAME}"
echo "comment: ${REAL_NAME}"
echo "password: ${PASSWORD}"
echo "hostname: ${HOSTNAME}"

exit 0