#!/bin/bash

# Check if user is root
echo "Your UID is: ${UID}"
ROOT_UID='0'
if [[ "${UID}" -ne "${VALID_UID}" ]]
then
    echo "ERROR: This script requires root privileges"
    exit 1
fi

## prompt for account name, real name & password
read -p "user name: " USER_NAME
read -p "real name: " COMMENT
read -p "pw: " PASSWORD

### creates account on the local system & set password
# $COMMENT may contain spaces, so we should wrap it inside of " "
useradd -c "${COMMENT}" -m ${USER_NAME}
# set password
echo ${PASSWORD} | passwd --stdin ${USER_NAME}

# force password change on 1st login
passwd -e ${USER_NAME}